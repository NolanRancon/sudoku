package vue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu {

    private JLabel labelMenu;
    private JButton choixNouvellePartie;
    private JButton choixReprendrePartie;
    private JButton choixHistorique;
    private JPanel panelMain;


    public Menu() {
        choixNouvellePartie.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SudokuGrid sudokuGrid = new SudokuGrid();
                sudokuGrid.setVisible(true);
                sudokuGrid.start(true);
            }
        });
        choixReprendrePartie.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SudokuGrid sudokuGrid = new SudokuGrid();
                sudokuGrid.setVisible(true);
                sudokuGrid.start(false);

            }
        });
        choixHistorique.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VueHistorique vue = new VueHistorique();
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Menu");
        frame.setContentPane(new Menu().panelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = screenSize.height;
        int width = screenSize.width;
        frame.setSize(width/2, height/2);
        frame.pack();
        // center the jframe on screen
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
