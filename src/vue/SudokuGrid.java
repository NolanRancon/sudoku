package vue;

import controller.DataBaseManager;
import model.GrilleSudoku;
import model.NumericAndLengthFilter;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Array;
import java.util.Arrays;


public class SudokuGrid extends JFrame {

    private JTextField f[][]= new JTextField[9][9] ;
    private JPanel p[][]= new JPanel [3][3];
    private JPanel sudoku = new JPanel();
    private JPanel button = new JPanel();
    private JButton resetButton = new JButton("Reinitialiser");
    private JPanel resetPanel = new JPanel();
    private JToggleButton lightDark = new JToggleButton("Light");
    private JPanel lightDarkPanel = new JPanel();

    private JButton solveButton = new JButton("Solution");
    private JPanel solvePanel = new JPanel();
    private DataBaseManager connect = new DataBaseManager();
    private GrilleSudoku grilleSudoku;
    private int[][] solveBoard;
    private int[][] gameBoard;
    private int[][] defaultBoard;
    private int id = -1;


    public SudokuGrid(){
        super("Sudoku");


        //Init JtextField with listener
        for(int x=0; x<=8; x++){
            for(int y=0; y<=8; y++){
                f[x][y]=new JTextField(1);
                f[x][y].setBackground(Color.white);
                f[x][y].setHorizontalAlignment(JTextField.CENTER);
                f[x][y].setFont(new Font("Bold", Font.BOLD, 14));
                f[x][y].getDocument().putProperty("x", x);
                f[x][y].getDocument().putProperty("y", y);
                f[x][y].getDocument().addDocumentListener(new DocumentListener() {
                    @Override
                    public void insertUpdate(DocumentEvent e) {
                        int x = (int)e.getDocument().getProperty("x");
                        int y = (int)e.getDocument().getProperty("y");
                        grilleSudoku.setGrilleValue(x,y,Integer.parseInt(f[x][y].getText()));

                        for (int i = 0; i < 9; i++) {
                            for (int j = 0; j < 9; j++) {
                                System.out.print(defaultBoard[i][j] + " ");
                            }
                            System.out.println("");
                        }

                    }

                    @Override
                    public void removeUpdate(DocumentEvent e) {
                        //Nothing to do
                    }

                    @Override
                    public void changedUpdate(DocumentEvent e) {
                        //Nothing to do
                    }
                });


                // Set Filter to JtextField to allow number between 1 n 9
                ((AbstractDocument) f[x][y].getDocument()).setDocumentFilter(new NumericAndLengthFilter(1));
            }
        }

        //Init sudoku's block
        for(int x=0; x<=2; x++){
            for(int y=0; y<=2; y++){
                p[x][y]=new JPanel(new GridLayout(3,3,3,3));
                p[x][y].setBackground(Color.black);
            }
        }

        //Init layout for the grid n the button
        sudoku.setLayout(new GridLayout(3,3,5,5));
        button.setLayout(new GridLayout(1,3,5,5));

        //add jtextfield to the block then to the grid
        for(int u=0; u<3; u++){
            for(int i=0; i<3; i++){
                for(int x=0; x<3; x++ ){
                    for(int y=0; y<3; y++){
                        p[u][i].add(f[x+u*3][y+i*3]);
                    }
                }
                sudoku.add(p[u][i]);
            }
        }

/*--------------------------------------------------------------------------------------------------------------------*/

        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("reinitialiser");
                try {
                    restart();
                } catch (BadLocationException ex) {
                    ex.printStackTrace();
                }
            }
        });

        resetButton.setSize(40, 15);
        resetPanel.add(resetButton);
        button.add(resetPanel);



        lightDark.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JToggleButton tBtn = (JToggleButton)e.getSource();
                if (tBtn.isSelected()) {
                    tBtn.setText("Dark");
                    dark();
                } else {
                    tBtn.setText("Light");
                    light(false);
                }
            }
        });

        lightDarkPanel.add(lightDark);
        button.add(lightDarkPanel);



        solveButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("solution");
                for (int i = 0; i < 9; i++) {
                    for (int j = 0; j < 9; j++) {
                        if (f[i][j].getText().equals("")){
                            f[i][j].setText(String.valueOf(solveBoard[i][j]));
                            i = 9;
                            j = 9;
                        }
                    }
                }
            }
        });

        solveButton.setSize(40,15);
        solvePanel.add(solveButton);
        button.add(solvePanel);



        sudoku.setPreferredSize(new Dimension(540,540));
        Border border = sudoku.getBorder();
        Border margin = new EmptyBorder(5,5,5,5);
        sudoku.setBorder(new CompoundBorder(border, margin));



        setLayout(new BorderLayout());
        add(sudoku);
        add(button, BorderLayout.SOUTH);
        setSize(600, 800);
        setResizable(false);
        light(true);
        pack();

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                int resp = JOptionPane.showConfirmDialog(SudokuGrid.this, "Voulez vous sauvegarder votre partie ?",
                        "Sauvegarder ?", JOptionPane.YES_NO_OPTION);

                if (resp == JOptionPane.YES_OPTION) {
                    save();
                    SudokuGrid.this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                } else {
                    SudokuGrid.this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                }
            }
        });

    }

    private void dark(){
        sudoku.setBackground(Color.black);
        button.setBackground(Color.darkGray);

        solvePanel.setBackground(Color.darkGray);
        resetPanel.setBackground(Color.darkGray);
        lightDarkPanel.setBackground(Color.darkGray);

        resetButton.setBackground(Color.gray);
        resetButton.setForeground(Color.black);
        solveButton.setBackground(Color.gray);
        solveButton.setForeground(Color.black);
        lightDark.setBackground(Color.gray);
        lightDark.setForeground(Color.black);

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                Color bg = f[i][j].getBackground();
                System.out.println(bg);
                if (bg == Color.gray){
                    f[i][j].setBackground(Color.white);
                } else if (bg == Color.white){
                    f[i][j].setBackground(Color.gray);
                }
            }
        }



    }

    private void light(boolean init){
        sudoku.setBackground(Color.white);
        button.setBackground(Color.white);

        solvePanel.setBackground(Color.white);
        resetPanel.setBackground(Color.white);
        lightDarkPanel.setBackground(Color.white);

        resetButton.setBackground(Color.lightGray);
        resetButton.setForeground(Color.black);
        solveButton.setBackground(Color.lightGray);
        solveButton.setForeground(Color.black);
        lightDark.setBackground(Color.lightGray);
        lightDark.setForeground(Color.black);

        if (!init){
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    Color bg = f[i][j].getBackground();
                    System.out.println(bg);
                    if (bg == Color.gray){
                        f[i][j].setBackground(Color.white);
                    } else if (bg == Color.white){
                        f[i][j].setBackground(Color.gray);
                    }
                }
            }
        }
    }


    public void start(boolean newGame){

        if(newGame){

            grilleSudoku = new GrilleSudoku(3);

            solveBoard = grilleSudoku.getSolution();

            defaultBoard = grilleSudoku.getDefaut();

            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    setNumber(i,j,defaultBoard[i][j]);
                }
            }

        } else {

            String[] res = connect.readGrille();

            String[][] defaultBoardString = Arrays.stream(res[0].substring(2, res[0].length() - 2).split("], \\["))
                    .map(e -> Arrays.stream(e.split("\\s*,\\s*"))
                            .toArray(String[]::new)).toArray(String[][]::new);

            String[][] gameBoardString = Arrays.stream(res[2].substring(2, res[2].length() - 2).split("], \\["))
                    .map(e -> Arrays.stream(e.split("\\s*,\\s*"))
                            .toArray(String[]::new)).toArray(String[][]::new);

            String[][] solveBoardString = Arrays.stream(res[1].substring(2, res[1].length() - 2).split("], \\["))
                    .map(e -> Arrays.stream(e.split("\\s*,\\s*"))
                            .toArray(String[]::new)).toArray(String[][]::new);

            solveBoard = new int[9][9];
            gameBoard = new int[9][9];
            defaultBoard = new int[9][9];
            id = Integer.parseInt(res[3]);

            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    defaultBoard[i][j] = Integer.parseInt(defaultBoardString[i][j]);
                    gameBoard[i][j] = Integer.parseInt(gameBoardString[i][j]);
                    solveBoard[i][j] = Integer.parseInt(solveBoardString[i][j]);
                }
            }

            grilleSudoku = new GrilleSudoku(defaultBoard, gameBoard, solveBoard);

            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    setNumber(i,j,defaultBoard[i][j]);
                    if (f[i][j].getText().equals("")){
                        f[i][j].setText(String.valueOf(gameBoard[i][j]));
                    }
                }

            }
        }
    }

    public void save(){
        gameBoard = grilleSudoku.getGrille();
        if (id == -1){
            connect.createGrille(defaultBoard, solveBoard, gameBoard);
        } else {
            connect.updateGrilleEnCours(id, gameBoard);
        }

    }

    public void restart() throws BadLocationException {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if(lightDark.getText().equals("Light")){
                    if (f[i][j].getBackground() != Color.gray){
                        f[i][j].getDocument().remove(0, f[i][j].getText().length());
                        grilleSudoku.setGrilleValue(i,j, 0);
                    }
                } else {
                    if(lightDark.getText().equals("Dark")){
                        if (f[i][j].getBackground() == Color.gray){
                            f[i][j].getDocument().remove(0, f[i][j].getText().length());
                            grilleSudoku.setGrilleValue(i,j, 0);
                        }
                    }
                }
            }
        }
    }

    public void setNumber(int x, int y, int number) {
        if (number > 0 && number < 10){
            f[x][y].setText(String.valueOf(number));
            f[x][y].setBackground(Color.gray);
            f[x][y].setEditable(false);
        }


    }


}