package controller;

import model.GrilleSudoku;
import model.Historique;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.*;
import java.util.Arrays;
import java.util.List;

public class DataBaseManager {
    Connection conn = null;

    public DataBaseManager() {

    }

    private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:C:\\Users\\ranco\\Desktop\\Prog\\Projet\\IdeaProjects\\BTSSIO2\\sudoku\\src\\sudoku.db";
        conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    public void close() {
        try {
            if (conn != null) {
                conn.close();
                conn = null;
            }
        } catch (SQLException e) {
            // e est en fait une liste d'exceptions
            while (e != null) {
                System.out.println("(1) SQLException: " + e.getMessage());
                System.out.println("(2) SQLState: " + e.getSQLState());
                System.out.println("(3) VendorError: " + e.getErrorCode());
                e = e.getNextException();
            }
        }
    }

    // lecture des grilles dans la bdd
    public String[] readGrille() {
        String sql = "SELECT * FROM sudoku ORDER BY id DESC LIMIT 1";
        String[] result = new String[4];

        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("id") + "\t" +
                        rs.getString("grilleJeux") + "\t" +
                        rs.getString("grilleSolution") + "\t" +
                        rs.getString("grilleEnCours"));

                result[0] = rs.getString("grilleJeux");
                result[1] = rs.getString("grilleSolution");
                result[2] = rs.getString("grilleEnCours");
                result[3] = String.valueOf(rs.getInt("id"));


            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return result;

    }

    //insertion des 3 grilles dans la bdd
    public void createGrille(int[][] grilleJeux, int[][] grilleSolution, int[][] grilleEnCours) {
        String sql = "INSERT INTO sudoku (grilleJeux, grilleSolution, grilleEnCours) VALUES (?,?,?)";
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, Arrays.deepToString(grilleJeux));
            pstmt.setString(2, Arrays.deepToString(grilleSolution));
            pstmt.setString(3, Arrays.deepToString(grilleEnCours));
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    // modifier grille en cours
    public void updateGrilleEnCours(int id, int[][] grilleEnCours) {
        String sql = "UPDATE sudoku SET grilleEnCours = ?  "
                + "WHERE id = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, Arrays.deepToString(grilleEnCours));
            pstmt.setInt(2, id);
            // update
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    // supprimer grille
    public void deleteGrille(int id) {
        String sql = "DELETE FROM sudoku WHERE id = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setInt(1, id);
            // execute the delete statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    // lecture de l'historique
    public void readHistorique() {
        String sql = "SELECT * FROM historique";

        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("idGrille") + "\t" +
                        rs.getInt("score"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


    // insertion score dans l'historique
    public void createHistorique(Historique idGrille, Historique score) {
        String sql = "INSERT INTO historique (idGrille, score) VALUES (?,?)";
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, idGrille.getIdGrille());
            pstmt.setInt(2, score.getScore());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    // modifier score dans l'historique
    public void updateScore(int idGrille, int score) {
        String sql = "UPDATE historique SET score = ?  "
                + "WHERE idGrille = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setInt(1, score);
            pstmt.setInt(2, idGrille);
            // update
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    // supprimer historique
    public void deleteHistorique(int id) {
        String sql = "DELETE FROM historique WHERE idGrille = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setInt(1, id);
            // execute the delete statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void vueHistorique() {
        String sql = "SELECT * FROM historique";

        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            String columns[] = { "Partie", "Score"};
            String data[][] = new String[50][2];

            int i = 0;
            while (rs.next()) {
                int id = rs.getInt("idGrille");
                int score = rs.getInt("score");
                data[i][0] = id + "";
                data[i][1] = score + "";
                i++;
            }
            DefaultTableModel model = new DefaultTableModel(data, columns);
            JTable table = new JTable(model);
            table.setShowGrid(true);
            table.setShowVerticalLines(true);
            JScrollPane pane = new JScrollPane(table);
            JFrame f = new JFrame("Historique");
            JPanel panel = new JPanel();
            panel.add(pane);
            f.add(panel);
            f.setSize(500, 250);
            f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            f.setVisible(true);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}

