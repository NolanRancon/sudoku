package model;

import java.lang.Math;
import java.util.Random;

public class GrilleSudoku{

    private int ordre;  // Ordre de la taille de la grille du sudoku
    private int taille;
    private int[][] defaut;
    private int[][] grille;   // grille du sudoku
    private int[][] solution;

    // Création de la grille de Sudoku à partir de son ordre
    public GrilleSudoku(int odr){
        this.ordre = odr;
        this.taille = (int) Math.pow(odr,2);  // Ex : ordre 3 -> taille 9
        // On génere et remplissons la grille avec GenerationGrille
        GenerationGrille();
    }

    // Création de la grille de Sudoku à partir d'un(e) tableau(grille) déjà crée
    public GrilleSudoku(int[][] d, int[][] g, int[][] s ){
        this.defaut = d;
        this.grille = g;
        this.solution = s;
        this.taille = this.grille.length;
        this.ordre = (int) Math.sqrt(this.taille);
    }

    // Création et remplissage de la Grille
    public void GenerationGrille(){
        this.grille = new int[taille][taille];
        this.defaut = new int[taille][taille];
        this.solution = new int[taille][taille];
        Random number = new Random();
        boolean test = false;
        int compteur = 0;
        int row, col,acc;
        while(!test && compteur < 200){
            InitSudoku();
            row = 0;
            col = 0;
            for(int i = 0; i < this.taille*2; i++){
                while(this.solution[row][col] != 0){
                    row = number.nextInt(this.taille);
                    col = number.nextInt(this.taille);

                }
                acc = number.nextInt(this.taille) + 1;
                while(!isAllowed(row, col, acc) && compteur < 200){
                    acc = number.nextInt(this.taille) + 1;
                    compteur++;
                }
                this.solution[row][col] = acc;
            }
            test = solveSudoku();
            compteur++;
        }

        row = 0;
        col = 0;
        if(test){
            for(int i = 0; i < this.taille*3; i++){
                while(this.grille[row][col] != 0 && compteur < 200){
                    row = number.nextInt(this.taille);
                    col = number.nextInt(this.taille);
                    compteur++;
                }
                this.grille[row][col] = this.solution[row][col];
            }
            this.defaut = this.grille;
        }
    }

    // initialise le contenue du sudoku à 0 (et sa solution)
    public void InitSudoku(){
        for(int i = 0; i < this.taille; i++){
            for(int j = 0; j < this.taille; j++){
                this.grille[i][j] = 0;
                this.solution[i][j] = 0;
            }
        }
    }


    //------------------------------------------------------------------------------
    // Ecriture dans le sudoku en solution

    // On vérifie qu'un nombre est possible dans une case
    public boolean isAllowed(int l, int c, int n){
        return !(containsInRow(l, n) || containsInCol(c, n) || containsInBloc(l, c, n));
    }

    // Vérifie la présence d'un nombre sur une ligne spécifique
    public boolean containsInRow(int row, int number){
        for(int j = 0; j < this.taille; j++){
            if(this.solution[row][j] == number){
                return true;
            }
        }
        return false;
    }

    // Vérifie la présence d'un nombre sur une colonne spécifique
    public boolean containsInCol(int col, int number){
        for(int i = 0; i < this.taille; i++){
            if(this.solution[i][col] == number){
                return true;
            }
        }
        return false;
    }

    // Vérifie la présence d'un nombre dans un bloc
    private boolean containsInBloc(int row, int col, int number){
        int r = row - row%3;
        int c = col - col%3;
        for(int i = r ; i < r+3 ; i++){
            for(int j = c; j < c+3 ; j++){
                if(this.solution[i][j] == number){
                    return true;
                }
            }

        }
        return false;
    }

    public void EcrireChiffre(int ligne, int colonne, int chiffre){
        boolean test = true;
        // on vérifie que les coordonnées et le chiffre soient dans la bonne plage (1 - taille)
        if (ligne < 0 || colonne < 0){
            test = false;
        }
        if (ligne > taille-1 || colonne > taille-1){
            test = false;
        }
        if (test){
            this.grille[ligne][colonne] = chiffre;
        }else{
            System.out.print("Erreur : Une des coordonnées est incorrect");
        }
    }

    //------------------------------------------------------------------------------
    // Résolution du sudoku

    public boolean solveSudoku(){
        for(int row = 0; row < 9; row++){
            for(int col = 0; col < 9; col++){
                if(this.solution[row][col] == 0){
                    for(int number = 1; number <= 9; number++){
                        if(isAllowed(row, col, number)){
                            this.solution[row][col] = number;
                            if(solveSudoku()){
                                return true;
                            }
                            else{
                                this.solution[row][col] = 0;
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    //----------------------------------------------------------------------------------------
    //Vérification de la validité d'un sudoku (tableaux) remplis

    // On vérifie si la grille complété est correct
    public boolean VerifGrille(){
        boolean res = true;

        // On vérifie que toute la grille est remplie
        /*
        Pour optimiser, on crée un compteur pour chaque chiffre ajouté à la grille.
        On vérifierai ensuite que le compteur soit égale au nombre de cellule pour vérifier qu'aucune ne soit vide
        */
        for (int i = 0; i < this.taille; i++){
            for (int j = 0; j < this.taille; j++){
                res = res && (this.grille[i][j] != 0);
                if (!res) break;
            }
            if (!res) break;
        }
        // Si la grille est remplie, on regarde si chaque nombre est unique sur sa ligne et sa colonne
        if(res){
            for(int i = 0; i < this.taille; i++){
                res = res && (VerifLigne(i));
                res = res && (VerifColonne(i));
            }
        }

        // En final, on vérifie que chaque nombre dans un bloc soit unique
        if(res){
            for(int i = 0; i < this.taille; i = i + this.ordre){
                for(int j = 0; j < this.taille; j = j + this.ordre){
                    res = res && VerifBloc(i,j);
                }
            }
        }

        return res;
    }

    // On vérifie que chaque nombre sur une ligne soit unique
    public boolean VerifLigne(int ligne){
        boolean res;
        res = UniqueValue(this.grille[ligne]);
        return res;
    }

    // On vérifie que chaque nombre sur une colonne soit unique
    public boolean VerifColonne(int colonne){
        boolean res;
        int[] tab = new int[this.taille];
        for(int i = 0; i < this.taille; i++){
            tab[i] = this.grille[i][colonne];
        }
        res = UniqueValue(tab);
        return res;
    }

    // On vérifie si chaque nombre dans bloc est unique
    // En entrée, on a les coordonnées de la case supérieur droit du bloc (line start/column start)
    public boolean VerifBloc(int ls, int cs){
        boolean res = true;
        int[] tab = new int[this.taille];
        int  acc;
        /*
        int cpt;
        */
        acc = 0;
        for(int i = ls; i < ls + this.ordre; i++){
            for(int j = cs; j < cs + this.ordre; j++){
                tab[acc] = this.grille[i][j];
                acc = acc + 1;
            }
        }
        res = UniqueValue(tab);
        return res;
    }

    // Cette fonction compte l'occurence d'un nombre dans un tableau
    public int CompteOccurence(int nombre, int[] tab){
        int occ = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i] == nombre){
                occ = occ + 1;
            }
        }
        return occ;
    }

    // Cette fonction renvoie vrai si chaque valeur du tableau est unique
    public boolean UniqueValue(int[] tab){
        boolean res = true;
        for(int i = 0; i < tab.length; i++){
            res = res && (CompteOccurence(tab[i],tab) == 1);
        }
        return res;
    }

    //-------------------------------------------------------------------------------------------

    public int[][] getGrille(){
        return this.grille;
    }

    public int[][] getDefaut() {
        return this.defaut;
    }

    public int[][] getSolution() {
        return this.solution;
    }

    public void setGrilleValue(int x, int y, int nombre){
        this.grille[x][y] = nombre;
    }

    public int getTaille(){
        return this.taille;
    }
    public int getOrdre(){
        return this.ordre;
    }


}





