package model;

public class Historique {
    private int idGrille;
    private int score;

    public Historique(int idGrille, int score){
        this.idGrille =idGrille;
        this.score = score;
    }

    public Historique(int idGrille){
        this.idGrille = idGrille;
        this.score = 0;
    }

    public int getIdGrille() {
        return idGrille;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
